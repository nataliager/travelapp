from django.db import models #models para crear tablas
from .user import User 

class Account(models.Model):

    #Atributos modelo Account
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='account', on_delete=models.CASCADE) #relacion uno a uno
    lastChangeDate = models.DateTimeField() #ultima fecha
    isActive = models.BooleanField(default=True)  #si esta activo o no   
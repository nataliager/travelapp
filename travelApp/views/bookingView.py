from django.conf import settings
from django.http.response import Http404
from travelApp.serializers.bookingSerializer import BookingSerializer
from travelApp.models.bookings import Bookings
from rest_framework.response import Response
from rest_framework import status, views
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated


class BookingView(views.APIView):

    permission_classes = (IsAuthenticated,)
    """
    Retrieve, update or delete a snippet instance.
    """
    #Valida si la reserva esta en la BD
    def get_object(self, pk):
        try:
            return Bookings.objects.get(pk=pk)
        except Bookings.DoesNotExist:
            raise Http404

    #Obtiene la info de una reserva en especifico por su pk = id
    def get(self, request, pk, format=None):

        token = request.META.get('HTTP_AUTHORIZATION')[7:] #obtener el token 
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM']) #traducir token, crea objeto tipo tokenBackend
        valid_data = tokenBackend.decode(token,verify=False)#decoficar token, info token

        booking = self.get_object(pk)
        serializer = BookingSerializer(booking)
        return Response(serializer.data)

    
    #Elimina una reserva de la BD segun su pk = id
    def delete(self, request, pk, format=None):

        token = request.META.get('HTTP_AUTHORIZATION')[7:] #obtener el token 
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM']) #traducir token, crea objeto tipo tokenBackend
        valid_data = tokenBackend.decode(token,verify=False)#decoficar token, info token

        booking = self.get_object(pk)
        booking.delete()
        return Response({"detail" : "La reserva fue eliminada exitosamente"},status=status.HTTP_204_NO_CONTENT)
    
    def post(self, request, *args, **kwargs): #solo metodo post

        token = request.META.get('HTTP_AUTHORIZATION')[7:] #obtener el token 
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM']) #traducir token, crea objeto tipo tokenBackend
        valid_data = tokenBackend.decode(token,verify=False)#decoficar token, info token

        serializer = BookingSerializer(data=request.data)#creacion obj UserSerializer-> json q llega
        serializer.is_valid(raise_exception=True)#verificar si la info esta correcta
        serializer.save()#guarda en la bd

        #Respuesta: codigo de respuesta CREADO
        return Response({"detail" : "La reserva fue exitosa"},status=status.HTTP_201_CREATED)
    

   

    
    
    